#!/usr/bin/env Rscript

# Dénes Türei EMBL & Uniklinik RWTH Aachen 2017
# turei.denes@gmail.com

# packages
require(dplyr)
require(plyr)
require(readr)
require(ggplot2)
require(stringr)

read_spectrum <- function(pathInput){
    
    exp <- str_match(pathInput, '([0-9]{3})_([A-Z]{1,2})')
    ex  <- exp[1,2]
    dye <- exp[1,3]
    
    (
        suppressMessages(suppressWarnings(
            read_tsv(pathInput, col_names = c('em', 'flu'))
        )) %>%
        filter(!is.na(as.numeric(em))) %>%
        mutate(em = as.numeric(em), flu = as.numeric(flu), ex = ex, dye = dye)
    )

}

dirData <- 'fluorometer'

vfnInput <- list.files(dirData, pattern = '.*txt')

data <-
    as_tibble(
        rbind.fill(
            lapply(
                vfnInput,
                function(fnInput){
                    read_spectrum(file.path(dirData, fnInput))
                }
            )
        )
    ) %>%
    mutate(
        dye = recode(
            dye,
            A  = 'A',
            CB = 'B',
            AB = 'AB'
        )
    )

p <- ggplot(data, aes(y = flu, x = em, color = dye)) +
    geom_line() +
    scale_color_manual(
        guide = guide_legend(title = 'Dye'),
        labels = c(
            'A'  = 'Alexa 594',
            'B'  = 'Cascade Blue',
            'AB' = 'Both'
        ),
        values = c(
            'A' = '#F9382D',
            'B' = '#1B7D8D',
            'AB' = 'gray25'
        )
    ) +
    annotate(xmin = 381.5, xmax = 392.5, ymin = -Inf, ymax = Inf, fill = '#1B7D8D', geom = 'rect', alpha = .2) +
    annotate(xmin = 417.0, xmax = 477.0, ymin = -Inf, ymax = Inf, fill = '#1B7D8D', geom = 'rect', alpha = .2) +
    annotate(xmin = 542.0, xmax = 582.0, ymin = -Inf, ymax = Inf, fill = '#F9382D', geom = 'rect', alpha = .2) +
    annotate(xmin = 603.5, xmax = 678.5, ymin = -Inf, ymax = Inf, fill = '#F9382D', geom = 'rect', alpha = .2) +
    geom_segment(aes(x = as.numeric(ex), xend = as.numeric(ex), y = -Inf, yend = Inf), lwd = 0.2, color = 'gray25', linetype = 4) +
    facet_wrap(~ex, nrow = 1) +
    xlab('Emission wavelength (nm)') +
    ylab('Fluorescence intensity') +
    ggtitle('Emission spectra of Cascade Blue and Alexa Fluor 594 separately and mixed (10 nM solutions)') +
    theme_linedraw() +
    theme(
        text = element_text(family = 'DINPro')
    )

ggsave('CB_A594_spectra.pdf', device = cairo_pdf, width = 16, height = 3)
