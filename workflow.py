#!/usr/bin/env python

# Dénes Türei EMBL & Uniklinik RWTH Aachen 2017
# turei.denes@gmail.com

import numpy as np
import barcoding

# pathRoot = '/home/denes/archive/20171129_droplet_barcoding/171121_CL15_day14_005/data'

pathRoot = '/home/denes/archive/microscopy/07022018_droplet_barcoding/171221_CL15_day14_002/data'

process1 = barcoding.Layers(pathRoot, sImgName = 'dapi_300.tif', sThisChannel = 'dapi')
process1.main()
