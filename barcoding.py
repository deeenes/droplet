#!/usr/bin/env python

# Dénes Türei EMBL & Uniklinik RWTH Aachen 2017-2018
# turei.denes@gmail.com

import os
import imp
import itertools

import numpy as np
import skimage.filters
import skimage.io
import skimage.morphology
import skimage.exposure
import skimage.filters
import skimage.feature
import skimage.segmentation
import sklearn.feature_extraction
import sklearn.cluster
from scipy import ndimage as ndi
import matplotlib.pyplot as plt
import seaborn as sns
import tqdm

colors = ['#1f77b4', '#ff7f0e', '#2ca02c', '#d62728',
          '#9467bd', '#8c564b', '#e377c2', '#7f7f7f',
          '#bcbd22', '#17becf']

sns.set(style = 'whitegrid', palette = colors, rc = {'axes.labelsize': 16})

class Layers(object):
    
    def __init__(self, pathRoot, sImgName = 'mcherry_300.tif', keep = False, **kwargs):
        
        self.pathRoot = pathRoot
        self.sImgName = sImgName
        self.keep = keep
        self.dLayerArgs = kwargs
        self.dLayers = {}
    
    def reload(self):
        modname = self.__class__.__module__
        mod = __import__(modname, fromlist = [modname.split('.')[0]])
        imp.reload(mod)
        new = getattr(mod, self.__class__.__name__)
        setattr(self, '__class__', new)
    
    def main(self):
        
        self.list_images()
        self.process()
    
    def list_images(self):
        
        self.lfnImages = [
            fnImg for fnImg
            in os.listdir(self.pathRoot)
            if fnImg.endswith(self.sImgName)
        ]
    
    def process(self):
        
        for fnImg in tqdm.tqdm(self.lfnImages, desc = 'Processing images', unit = 'image'):
            
            pathImg = os.path.join(self.pathRoot, fnImg)
            thisLayer = Layer(pathImg, **self.dLayerArgs)
            thisLayer.main()
            
            if self.keep:
                
                self.dLayers[fnImg] = thisLayer


class Layer(object):
    
    def __init__(self,
                 pathImg,
                 funIntensityMethods = {
                     'pct90': lambda a: np.percentile(a, q = 90),
                     'mean': lambda a: np.mean(a),
                     'med': lambda a: np.median(a),
                     'sum': lambda a: np.sum(a)
                 },
                 iLittleSize = 1000,
                 iRawValueOffset = 32768,
                 iMedFilterSize = 6,
                 sThisChannel = 'mcherry',
                 sLabel = ''):
        
        self.sLabel = sLabel
        self.pathImg = pathImg
        self.pathRoot = os.path.join(*os.path.split(pathImg)[:-1])
        self.iRawValueOffset = iRawValueOffset
        self.iMedFilterSize = iMedFilterSize
        self.iLittleSize = iLittleSize
        self.sThisChannel = sThisChannel
        self.sOtherChannel = (
            'dapi'
            if sThisChannel == 'mcherry'
            else 'mcherry'
        )
        self.pathOtherImg = self.pathImg.replace(
            self.sThisChannel, self.sOtherChannel)
        self.dddfSegInt = {}
        self.fnData = os.path.join(self.pathRoot, 'values%s' % self.sLabel)
        self.funIntensityMethods = funIntensityMethods
    
    def reload(self):
        modname = self.__class__.__module__
        mod = __import__(modname, fromlist=[modname.split('.')[0]])
        imp.reload(mod)
        new = getattr(mod, self.__class__.__name__)
        setattr(self, '__class__', new)
    
    def main(self):
        
        self.read()
        self.median_filter()
        self.threshold_first_shoulder()
        self.segment_by_threshold()
        self.segment_watershed()
        self.region_properties()
        self.remove_little()
        self.dddfSegInt[self.sThisChannel] = self.segments_intensity()
        self.other_image()
        self.dddfSegInt[self.sOtherChannel] = self.segments_intensity()
        self.plot_threshold()
        self.append_data()
    
    def read(self):
        
        self.aImgRaw = self._read(self.pathImg)
    
    def _read(self, path):
        
        img = skimage.io.imread(path)
        img = img - self.iRawValueOffset
        img = img - img.min()
        
        return img
    

    def median_filter(self):
        """
        Do a median filter to remove little spots (noise).
        """
        select = skimage.morphology.square(self.iMedFilterSize)
        self.aImg = skimage.filters.median(self.aImgRaw, select)

    def threshold_first_shoulder(self):
        """
        Finds a threshold above the leftmost peak of histogram.
        """
        
        self.iThreshold = np.inf
        self.aHist, self.aBins = skimage.exposure.histogram(self.aImg)
        
        vprev = -np.inf
        rprev = -np.inf
        for v, x in zip(self.aHist, self.aBins):
            
            r = vprev / v
            
            if vprev > v:
                
                if rprev > r:
                    
                    self.iThreshold = x
                    break
            
            rprev = vprev / v
            vprev = v
    
    def segment_by_threshold(self):
        """
        Defines segment by a threshold.
        """
        
        self.aTSegm = self.aImg >= self.iThreshold
    
    def segment_watershed(self):
        
        self.aTSegmDist = ndi.distance_transform_edt(self.aTSegm)
        self.aTSegmLocMax = skimage.feature.peak_local_max(
            self.aTSegmDist,
            indices = False,
            footprint = np.ones((3, 3)),
            labels = self.aTSegm
        )
        self.aTSegmMark = ndi.label(self.aTSegmLocMax)[0]
        self.aWshedLabs = skimage.morphology.watershed(
            -self.aTSegmDist,
            self.aTSegmMark,
            mask = self.aTSegm
        )
    
    def region_properties(self, img = None):
        
        img = self.aImgRaw if img is None else img
        
        self.lrpRegProp = skimage.measure.regionprops(
            self.aWshedLabs,
            intensity_image = img
        )
    
    def remove_little(self):
        """
        Removes too small segments.
        """
        
        self.aWSegm = self.aWshedLabs > 0
        
        for rp in self.lrpRegProp:
            
            if rp.area < self.iLittleSize:
                
                self.aWSegm[self.aWshedLabs == rp.label] = 0
                self.aWshedLabs[self.aWshedLabs == rp.label] = 0
        
        self.region_properties()
    
    def segments_intensity(self):
        
        ddfSegInt = {}
        
        for label, method in self.funIntensityMethods.items():
            
            ddfSegInt[label] = {}
            
            for rp in self.lrpRegProp:
                
                ddfSegInt[label][rp.label] = method(
                    rp.intensity_image[rp.intensity_image > 0]
                )
        
        return ddfSegInt
    
    def other_image(self):
        
        self.aImg2 = self._read(self.pathOtherImg)
        self.region_properties(self.aImg2)
    
    def plot_threshold(self):
        """
        A series of plots for quality control of the whole process.
        """
        
        self.pathPdfThreshold = '%s--threshold.pdf' % self.pathImg
        
        fig, ax = plt.subplots(1, 8, figsize = (40, 5))
        
        with sns.axes_style('dark'):
            
            ax[0].imshow(self.aImg, cmap = plt.cm.viridis)
            ax[1].imshow(self.aImg, cmap = plt.cm.gray)
            ax[2].imshow(self.aTSegm)
            ax[3].imshow(self.aTSegmDist, cmap = plt.cm.gray, interpolation = 'nearest')
            ax[4].imshow(self.aWshedLabs,
                         cmap = plt.cm.spectral,
                         interpolation = 'nearest')
            #ax[3].imshow(skimage.segmentation.mark_boundaries(self.aImg, self.aWshedLabs, mode = 'inner'))
            ax[5].imshow(self.aWSegm, cmap = plt.cm.gray)
        
        ax[-2].set_xlabel('Pixel value')
        ax[-2].set_ylabel('Count (log)')
        ax[-2].set_yscale('log')
        ax[-2].fill_between(self.aBins, self.aHist, alpha = .75)
        ax[-2].plot(
            [self.iThreshold, self.iThreshold],
            [0, self.aHist.max()],
            color = colors[1]
        )
        
        methods = sorted(self.funIntensityMethods.keys())
        self.lRegLab = sorted(self.dddfSegInt[self.sThisChannel][methods[0]].keys())
        
        if self.lRegLab:
            
            sns.regplot(
                x = np.array([self.dddfSegInt[self.sThisChannel][methods[0]][l]
                              for l in self.lRegLab]),
                y = np.array([self.dddfSegInt[self.sOtherChannel][methods[0]][l]
                              for l in self.lRegLab]),
                ax = ax[-1],
                fit_reg = False
            )
            
            ax[-1].set_xlabel(self.sThisChannel)
            ax[-1].set_ylabel(self.sOtherChannel)
            ax[-1].set_xscale('log')
            ax[-1].set_yscale('log')
        
        plt.savefig(self.pathPdfThreshold)
        plt.close()
    
    def append_data(self):
        
        methods = sorted(self.funIntensityMethods.keys())
        hdr  = ['well', 'photo', 'droplet']
        hdr.extend(['%s_%s' % (self.sThisChannel, m) for m in methods])
        hdr.extend(['%s_%s' % (self.sOtherChannel, m) for m in methods])
        well = os.path.split(self.pathImg)[-1].split('--')[0]
        photo = os.path.split(self.pathImg)[-1].split('--')[2]
        
        with open(self.fnData, 'a') as fp:
            
            #print('\n\n\n\n', fp.tell(), '\n\n\n\n')
            if fp.tell() == 0:
                fp.write('%s\n' % '\t'.join(hdr))
            
            fp.write(
                '\n'.join(
                    '\t'.join(itertools.chain([
                        well,
                        photo,
                        '%u' % l
                    ],
                    [
                        '%.08f' % self.dddfSegInt[self.sThisChannel][method][l]
                        for method in methods
                    ],
                    [
                        '%.08f' % self.dddfSegInt[self.sOtherChannel][method][l]
                        for method in methods
                    ]))
                    for l in self.lRegLab
                )
            )
            if len(self.lRegLab):
                fp.write('\n')
