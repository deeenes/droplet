#!/usr/bin/env Rscript

# Dénes Türei EMBL & Uniklinik RWTH Aachen 2017
# turei.denes@gmail.com

# packages
require(dplyr)
require(readr)
require(ggplot2)
require(tidyr)

inFile <- 'denes_20171207_A594_CB.csv'

data <-
    suppressWarnings(
        read_tsv(
            inFile,
            col_types = cols(
                .default = col_character(),
                c1 = col_number(),
                c2 = col_number(),
                c3 = col_number(),
                c4 = col_number(),
                c5 = col_number(),
                ex = col_number(),
                em = col_number()
            )
        )
    ) %>%
    gather(
        key = column,
        value = flu,
        c1, c2, c3, c4, c5, c6, c7
    ) %>%
    mutate(
        conc = ifelse(
            column %in% c('c5', 'c6', 'c7') & row == 'e', 0, ifelse(
            column %in% c('c2', 'c3', 'c4') & row == 'e', NA, ifelse(
            column %in% c('c2', 'c5'), 100, ifelse(
            column %in% c('c3', 'c6'), 50, ifelse(
            column %in% c('c4', 'c7'), 10, NA
        ))))),
        ex_em = sprintf('%i_%i', ex, em),
        flu = as.numeric(flu)
    ) %>%
    filter(!is.na(conc) & row != 'a' & ex != 398)

zeros <- data %>%
    filter(
        conc == 0
    )

data <- bind_rows(
    data %>% filter(conc != 0),
    zeros %>% mutate(dye = 'A'),
    zeros %>% mutate(dye = 'B'),
    zeros %>% mutate(dye = 'AB')
)

p <- ggplot(data, aes(y = flu, x = as.factor(conc), color = dye)) +
    #geom_point() +
    geom_boxplot(lwd = .1, outlier.size = .1) +
    facet_grid(em ~ ex) +
    #scale_y_continuous(limits = c(0, 200)) +
    scale_shape_manual(
        guide = guide_legend(title = 'Dye'),
        labels = c(
            'A'  = 'Alexa 594',
            'B'  = 'Cascade Blue',
            'AB' = 'Both'
        ),
        values = c(
            'A' = 2,
            'B' = 6,
            'AB' = 1
        )
    ) +
    scale_color_manual(
        guide = guide_legend(title = 'Dye'),
        labels = c(
            'A'  = 'Alexa 594',
            'B'  = 'Cascade Blue',
            'AB' = 'Both'
        ),
        values = c(
            'A' = '#F9382D',
            'B' = '#1B7D8D',
            'AB' = 'gray25'
        )
    ) +
    xlab('Dye concentration (nM)') +
    ylab('Fluorescence intensity') +
    ggtitle(paste0(
        'Fluorescence of Cascade Blue and Alexa 594 under various\n',
        'excitation and emission wavelengths (columns vs. rows) in plate reader')) +
    theme_linedraw() +
    theme(
        text = element_text(family = 'DINPro')
    )

ggsave('platereader_CB_A594.pdf', device = cairo_pdf, width = 9, height = 9)
