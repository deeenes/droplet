#!/usr/bin/env Rscript

# Dénes Türei EMBL & Uniklinik RWTH Aachen 2017
# turei.denes@gmail.com

# packages
require(dplyr)
require(readr)
require(ggplot2)

# constants
# dirRoot <- '/home/denes/archive/20171129_droplet_barcoding/171121_CL15_day14_005/data/'
dirRoot <- '/home/denes/archive/microscopy/07022018_droplet_barcoding/171221_CL15_day14_002/data/'
cMethods  <- c('pct90', 'mean', 'sum')
vCols  <- c('well', 'photo', 'droplet', 'mcherry', 'dapi')
segmChannel <- 'dapi300'
# preprocessing
fnData <- paste0(dirRoot, 'values')
data <- suppressMessages(read_tsv(fnData))

# plots
for(method in cMethods){
    
    fnPdf  <- paste0(dirRoot, sprintf('barcoding_%s_%s.pdf', segmChannel, method))
    fnD1   <- paste0(dirRoot, sprintf('dens_A594_%s_%s.pdf', segmChannel, method))
    fnD2   <- paste0(dirRoot, sprintf('dens_CB_%s_%s.pdf', segmChannel, method))
    
    p <- ggplot(data,
            aes_string(
                x = sprintf('mcherry_%s', method),
                y = sprintf('dapi_%s', method)
            )
        ) +
        geom_point(alpha = .4) +
        scale_x_log10() +
        scale_y_log10() +
        xlab('Alexa 594') +
        ylab('Cascade Blue') +
        ggtitle('Intensities of barcoding dyes in individual droplets') +
        theme_linedraw() +
        theme(
            text = element_text(family = 'DINPro')
        )

    ggsave(fnPdf, device = cairo_pdf, width = 8, height = 8)

    p <- ggplot(data, aes_string(sprintf('mcherry_%s', method))) +
        geom_density() +
        scale_x_log10() +
        xlab('Alexa 594') +
        ylab('Density') +
        ggtitle('Smoothed density of Alexa 594 intensities') +
        theme_linedraw() +
        theme(
            text = element_text(family = 'DINPro')
        )

    ggsave(fnD1, device = cairo_pdf, width = 5, height = 5)


    p <- ggplot(data, aes_string(sprintf('dapi_%s', method))) +
        geom_density() +
        scale_x_log10() +
        xlab('Cascade Blue') +
        ylab('Density') +
        ggtitle('Smoothed density of Cascade Blue intensities') +
        theme_linedraw() +
        theme(
            text = element_text(family = 'DINPro')
        )

    ggsave(fnD2, device = cairo_pdf, width = 5, height = 5)
    
}
